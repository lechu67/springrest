package pl.leszek.recruitment.pac.springrest.domain.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import pl.leszek.recruitment.pac.springrest.domain.user.form.UserRegistrationForm;
import pl.leszek.recruitment.pac.springrest.domain.user.form.UserUpdateForm;
import pl.leszek.recruitment.pac.springrest.domain.user.model.User;
import pl.leszek.recruitment.pac.springrest.domain.user.repository.UserRepository;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtests.properties")
class UserControllerTest {
    private static final String USERS_ROOT_URL = "/api/v1/users/";
    private static final String EMAIL = "email@test.com";
    private static final String EMAIL_2 = "email2@test.com";
    private static final String EMAIL_3 = "email3@test.com";
    private static final String FIRST_NAME = "firstName";
    private static final String FIRST_NAME_2 = "firstName2";
    private static final String LAST_NAME = "lastName";
    private static final String LAST_NAME_2 = "lastName2";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private UserRepository repository;



    @Test
    @SneakyThrows
    void shouldCreateUser() {
        UserRegistrationForm form = new UserRegistrationForm(FIRST_NAME, LAST_NAME, EMAIL);

        String result = mockMvc
                .perform(post(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString();
        assertTrue(repository.existsById(Long.valueOf(result)));
    }

    @Test
    @SneakyThrows
    void shouldNotCreateUserWithSameEmail() {
        UserRegistrationForm form = new UserRegistrationForm(FIRST_NAME, LAST_NAME, EMAIL_3);

        mockMvc
                .perform(post(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isCreated());

        mockMvc
                .perform(post(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @SneakyThrows
        //this is an example of validation test, there should be more. But honestly in real world we rarely have time to write such tests.
    void shouldNotCreateUserWithoutFirstName() {
        UserRegistrationForm form = new UserRegistrationForm(null, LAST_NAME, EMAIL);

        mockMvc
                .perform(post(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isBadRequest());
    }

    @Test
    @SneakyThrows
    void shouldRetrieveUser() {
        User savedUser = repository.save(createUser(EMAIL_2, null, null));

        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email", is(EMAIL_2)))
                .andExpect(jsonPath("id", notNullValue()));
    }

    @Test
    @SneakyThrows
    void shouldNotRetrieveNonExistentUser() {
        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", 123))
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    void shouldUpdateUser() {
        User userToSave = createUser(EMAIL, FIRST_NAME, LAST_NAME);
        User savedUser = repository.save(userToSave);

        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email", is(EMAIL)))
                .andExpect(jsonPath("id", notNullValue()));

        UserUpdateForm form = new UserUpdateForm();
        form.setId(savedUser.getId());
        form.setEmail(EMAIL_2);
        form.setFirstName(FIRST_NAME_2);
        form.setLastName(LAST_NAME_2);

        mockMvc
                .perform(put(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isNoContent());

        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("email", is(EMAIL_2)))
                .andExpect(jsonPath("firstName", is(FIRST_NAME_2)))
                .andExpect(jsonPath("lastName", is(LAST_NAME_2)))
                .andExpect(jsonPath("id", is(savedUser.getId().intValue())));
    }

    @Test
    @SneakyThrows
    void shouldNotUpdateUserNonExistingUser() {
        UserUpdateForm form = new UserUpdateForm();
        form.setId(123L);
        form.setEmail(EMAIL_2);
        form.setFirstName(FIRST_NAME_2);
        form.setLastName(LAST_NAME_2);

        mockMvc
                .perform(put(USERS_ROOT_URL)
                        .contentType(APPLICATION_JSON)
                        .content(mapper.writeValueAsString(form)))
                .andExpect(status().isNotFound());
    }

    @Test
    @SneakyThrows
    void shouldDeleteUser() {
        User userToSave = createUser(EMAIL, FIRST_NAME, LAST_NAME);
        User savedUser = repository.save(userToSave);
        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isOk());

        mockMvc
                .perform(delete(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isNoContent());

        mockMvc
                .perform(get(USERS_ROOT_URL + "{id}", savedUser.getId()))
                .andExpect(status().isNotFound());

        assertFalse(repository.existsById(savedUser.getId()));
    }

    private User createUser(String email, String firstName, String lastName) {
        User userToSave = new User();
        userToSave.setEmail(email);
        userToSave.setLastName(lastName);
        userToSave.setFirstName(firstName);
        return userToSave;
    }
}