package pl.leszek.recruitment.pac.springrest.domain.user.service.impl;

import ma.glasnost.orika.MapperFacade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.leszek.recruitment.pac.springrest.domain.user.dto.UserTO;
import pl.leszek.recruitment.pac.springrest.domain.user.exception.UserEmailNotAvailableException;
import pl.leszek.recruitment.pac.springrest.domain.user.exception.UserNotFoundException;
import pl.leszek.recruitment.pac.springrest.domain.user.model.User;
import pl.leszek.recruitment.pac.springrest.domain.user.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    public static final String EMAIL = "email@test.com";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final long ID = 123L;
    public static final long NON_EXISTING_ID = 234L;

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private MapperFacade mapper;

    private User user;
    private UserTO userTO;

    @BeforeEach
    void init() {
        user = new User();
        user.setEmail(EMAIL);
        user.setId(ID);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);

        userTO = new UserTO();
        userTO.setId(ID);
        userTO.setEmail(EMAIL);
        userTO.setFirstName(FIRST_NAME);
        userTO.setLastName(LAST_NAME);
    }


    @Test
    void shouldFindUserById() {
        when(userRepository.findById(ID)).thenReturn(Optional.of(user));
        when(mapper.map(user, UserTO.class)).thenReturn(userTO);

        UserTO result = userService.findById(ID);

        assertEquals(result.getEmail(), user.getEmail());
    }

    @Test
    void shouldThrow404WhenUserNotFound() {
        when(userRepository.findById(NON_EXISTING_ID)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.findById(NON_EXISTING_ID));
    }

    @Test
    void shouldCreateUser() {
        when(userRepository.existsByEmail(EMAIL)).thenReturn(false);
        when(mapper.map(userTO, User.class)).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        Long result = userService.createUser(userTO);

        verify(userRepository, times(1)).save(user);
        assertEquals(user.getId(), result);
    }

    @Test
    void shouldNotCreateUserWhenExists() {
        when(userRepository.existsByEmail(EMAIL)).thenReturn(true);

        assertThrows(UserEmailNotAvailableException.class, () -> userService.createUser(userTO));
    }

    @Test
    void shouldUpdateUser() {
        when(userRepository.findById(ID)).thenReturn(Optional.of(user));

        userService.updateUser(userTO);

        verify(userRepository, times(1)).save(user);
    }

    @Test
    void shouldNotUpdateUserWhenNotFound() {
        when(userRepository.findById(ID)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> userService.updateUser(userTO));
    }

    @Test
    void shouldDeleteUser() {
        when(userRepository.findById(ID)).thenReturn(Optional.of(user));

        userService.deleteUser(ID);

        verify(userRepository).deleteById(ID);
    }
}