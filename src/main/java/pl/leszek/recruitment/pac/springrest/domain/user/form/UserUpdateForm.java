package pl.leszek.recruitment.pac.springrest.domain.user.form;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserUpdateForm {

    @NotNull
    private Long id;
    @NotEmpty
    @Size(max = 30)
    private String firstName;
    @NotEmpty
    @Size(max = 30)
    private String lastName;
    @Email
    @Size(max = 50)
    @NotEmpty
    private String email;
}
