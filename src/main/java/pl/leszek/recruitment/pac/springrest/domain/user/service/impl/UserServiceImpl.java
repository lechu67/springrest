package pl.leszek.recruitment.pac.springrest.domain.user.service.impl;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.stereotype.Service;
import pl.leszek.recruitment.pac.springrest.domain.user.dto.UserTO;
import pl.leszek.recruitment.pac.springrest.domain.user.exception.UserEmailNotAvailableException;
import pl.leszek.recruitment.pac.springrest.domain.user.exception.UserNotFoundException;
import pl.leszek.recruitment.pac.springrest.domain.user.model.User;
import pl.leszek.recruitment.pac.springrest.domain.user.repository.UserRepository;
import pl.leszek.recruitment.pac.springrest.domain.user.service.UserService;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final MapperFacade mapper;

    @Override
    public UserTO findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return mapper.map(user, UserTO.class);
    }

    @Override
    public Long createUser(UserTO userTO) {
        if (userRepository.existsByEmail(userTO.getEmail())) {
            throw new UserEmailNotAvailableException();
        }
        return userRepository.save(mapper.map(userTO, User.class)).getId();
    }

    @Override
    public void updateUser(UserTO userTO) {
        User user = userRepository.findById(userTO.getId()).orElseThrow(UserNotFoundException::new);
        mapper.map(userTO, user);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.findById(id).ifPresent(u -> userRepository.deleteById(id));
    }
}
