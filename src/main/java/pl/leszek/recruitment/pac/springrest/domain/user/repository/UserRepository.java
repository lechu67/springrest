package pl.leszek.recruitment.pac.springrest.domain.user.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import pl.leszek.recruitment.pac.springrest.domain.user.model.User;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    boolean existsByEmail(String email);
}
