package pl.leszek.recruitment.pac.springrest.domain.user.service;

import pl.leszek.recruitment.pac.springrest.domain.user.dto.UserTO;

public interface UserService {
    UserTO findById(Long id);

    Long createUser(UserTO userTO);

    void updateUser(UserTO userTO);

    void deleteUser(Long id);
}
