package pl.leszek.recruitment.pac.springrest.domain.user.controller;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.leszek.recruitment.pac.springrest.domain.user.dto.UserTO;
import pl.leszek.recruitment.pac.springrest.domain.user.form.UserRegistrationForm;
import pl.leszek.recruitment.pac.springrest.domain.user.form.UserUpdateForm;
import pl.leszek.recruitment.pac.springrest.domain.user.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final MapperFacade mapper;

    @GetMapping("{id}")
    public ResponseEntity<UserTO> retrieveUser(@PathVariable Long id) {
        return ResponseEntity.ok(userService.findById(id));
    }

    @PostMapping
    public ResponseEntity<Long> createUser(@RequestBody @Valid UserRegistrationForm userForm) {
        return new ResponseEntity<>(userService.createUser(mapper.map(userForm, UserTO.class)), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> updateUser(@RequestBody @Valid UserUpdateForm userForm) {
        userService.updateUser(mapper.map(userForm, UserTO.class));
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
