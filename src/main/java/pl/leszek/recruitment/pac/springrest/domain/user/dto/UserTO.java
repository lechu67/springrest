package pl.leszek.recruitment.pac.springrest.domain.user.dto;

import lombok.Data;

@Data
public class UserTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
