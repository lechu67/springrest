package pl.leszek.recruitment.pac.springrest.domain.user.form;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class UserRegistrationForm {
    @NotEmpty
    @Size(max = 30)
    private String firstName;
    @NotEmpty
    @Size(max = 30)
    private String lastName;
    @Email
    @NotEmpty
    @Size(max = 50)
    private String email;
}
