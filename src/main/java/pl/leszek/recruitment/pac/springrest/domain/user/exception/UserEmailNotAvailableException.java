package pl.leszek.recruitment.pac.springrest.domain.user.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "USER_EMAIL_NOT_AVAILABLE_EXCEPTION")
public class UserEmailNotAvailableException extends RuntimeException {
}
